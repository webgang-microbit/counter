from microbit import *

iCounter = 0

while True:
    if button_a.is_pressed():
        iCounter = iCounter - 1
        # show some reaction
        display.show("-")
        # give time to react and not count on too fast
        sleep(100)
    if button_b.is_pressed():
        iCounter = iCounter + 1
        # show some reaction        
        display.show("+")
        # give time to react and not count on too fast
        sleep(100)
    display.show(str(iCounter))
    sleep(100)